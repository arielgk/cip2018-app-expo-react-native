import React from 'react';

import {Text, View, Button} from 'react-native';
import {createStackNavigator} from 'react-navigation';

import {Provider} from "react-redux";
import configureStore from "./src/store/configureStore";
import initialState from "./src/store/initialState";
import Home from './src/components/Home';
import News from './src/components/News';
import AgendaContainer from './src/components/Events/AgendaContainer';
import Details from './src/components/Events/Details';
import Maps from './src/components/Maps';
import {PersistGate} from 'redux-persist/integration/react'

const {store,persistor }= configureStore(initialState);

const initConfig = store.getState();
console.log(store);
const Navigator = createStackNavigator(
    {
        Home: {
            screen: Home,
        },
        News: {
            screen: News,

        },
        Events: {
            screen: AgendaContainer,
        },
        Details: {
            screen: Details,
        },
        Maps: {
            screen: Maps,
        }

    },
    initConfig.config
);

class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <Navigator/>
                </PersistGate>
            </Provider>
        );
    }
}

export default App;




import React from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {
    ScrollView,
    Modal,
    TouchableHighlight,
    TouchableOpacity,
    StyleSheet,
    Text,
    View,
    Button,
    Image
} from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import piso_1 from '../../../assets/piso_1.jpg';
import piso_2 from '../../../assets/piso_2.jpg';

const images = [
     {
        props: {
            // Or you can set source directory.
            source: piso_1,
        }
    },

    {
        props: {
            source: piso_2,
        }
    }]

class Maps extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            modalVisible: true
        };
    }

    setModalVisible(visible) {
        // this.setState({modalVisible: visible});
        // this.props.navigation.push('Home');
        this.props.toggleMap();
    }

    render() {
        return (
            <View
                style={{
                    padding: 10,
                    backgroundColor: '#fff',
                }}
            >
                <Modal

                    visible={this.state.modalVisible}
                    transparent={false}
                    enableSwipeDown="true"
                    onRequestClose={() => this.setState({modalVisible: false})}
                >

                    <ImageViewer imageUrls={images} index={this.state.index}/>
                    <TouchableHighlight
                        onPress={() => {
                            this.setModalVisible(!this.state.modalVisible);
                        }}>
                        <Text style={styles.closemodal}>Cerrar</Text>
                    </TouchableHighlight>

                </Modal>
            </View>
        )
    }
}

Maps.propTypes = {
    config: PropTypes.object.isRequired,
}

const mapStateToProps = (state, ownProps) => {
    return {
        config: state.config,

    }
}

const styles = StyleSheet.create({
    closemodal: {
        height: 50,
        zIndex: 1,
        textAlign: 'center',
        fontSize: 18,
        padding: 10,

    }
});

export default connect(mapStateToProps)(Maps)
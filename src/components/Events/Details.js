import React from 'react';
import PropTypes from 'prop-types';
import {
    ActivityIndicator,
    TouchableOpacity,
    Button,
    ScrollView,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions
} from 'react-native';
import Expo from 'expo';
import {connect} from 'react-redux';
import ResponsiveImageView from 'react-native-responsive-image-view';
import {globals} from '../Globals';
import * as favouritesActions from "../../actions/favouritesActions";
import {bindActionCreators} from "redux";

class Details extends React.Component {
    constructor(props) {
        super(props);
        this.createCalendar = this.createCalendar.bind(this);
    }

    static navigationOptions = ({navigation, navigationOptions}) => {
        const {params} = navigation.state;

        return {
            title: params ? params.otherParam : 'Detalles',
            /* These values are used instead of the shared configuration! */
            headerStyle: {
                backgroundColor: navigationOptions.headerTintColor,
            },
            headerTintColor: navigationOptions.headerStyle.backgroundColor,
        };
    };

    createCalendar(event) {
        console.log(event);
        const calendar = Expo.Calendar.createEventAsync(Expo.Calendar.DEFAULT, {
            title: event.title,
            endDate: "2018-06-13T07:44:24.088-05:00",
            startDate: "2018-06-13T08:00:24.496-05:00",
            allDay: false,
            location: event.venue_name,
            timeZone: 'UTC -3',
            endTimeZone: 'UTC -3',
        });
        console.log(calendar);

    }

    render() {

        const {title, authors, startTime, endTime, category, eje, simposio, start, end, venue_name, actions, favourite} = this.props;
        console.log(actions);
        return (
            <ScrollView style={styles.container}>

                <View style={styles.description}>
                    <Text style={styles.title}>{title}</Text>
                    <Text style={styles.time}>{startTime} - {endTime}</Text>
                    <Text style={styles.venue}> {venue_name}</Text>
                    <Text style={styles.detailHeader}>Tipo: </Text>
                    <Text>{category}</Text>
                    <Text style={styles.detailHeader}>Autores: </Text>
                    <Text>{authors}</Text>

                    {eje !== '' ?
                        <View>
                            <Text style={styles.detailHeader}>Eje Temático</Text>
                            <Text>{eje}</Text>
                        </View>
                        :
                        <View><Text/></View>}
                    {simposio !== '' ?
                        <View>
                            <Text style={styles.detailHeader}>Simposio</Text>
                            <Text>{simposio}</Text>
                        </View>
                        :
                        <View><Text/></View>}

                    {favourite === false ? <Button title={"Añadir a favoritos"} onPress={() => {
                        actions.setFavourite({
                            title: title,
                            startTime: startTime,
                            endTime: endTime,
                            start: start,
                            authors: authors,
                            venue_name: venue_name,

                        })
                    }}/> : <Text/>}

                    {/*<View>*/}
                        {/*<TouchableOpacity onPress={() => {*/}
                            {/*this.createCalendar({*/}
                                {/*title: title,*/}
                                {/*startTime: startTime,*/}
                                {/*endTime: endTime,*/}
                                {/*start: start,*/}
                                {/*end: end,*/}
                                {/*venue_name: venue_name,*/}
                            {/*})*/}
                        {/*}}>*/}
                            {/*<Text>Añadir a Calandario</Text>*/}

                        {/*</TouchableOpacity>*/}

                    {/*</View>*/}


                </View>
            </ScrollView>
        );
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    badge: {
        alignItems: 'center',
        padding: 15
    },
    badgeInfo: {
        alignItems: 'center',
        flex: 1
    },
    avatar: {
        width: 100,
        height: 100,
        borderRadius: 50,
        borderColor: globals.colors.primary,
        borderWidth: 4,
        marginBottom: 15,
    },
    name: {
        fontSize: 18,
        textAlign: 'center',
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 8
    },
    time: {
        fontSize: 14,
        textAlign: 'center',
        color: globals.colors.lightGrey,
        marginBottom: 15
    },
    venue:{
        fontSize: 25,
        textAlign: 'center',
        color: globals.colors.primary,
        marginBottom: 14
    },
    description: {

        flex: 1,
        padding: 15,
    },
    detailHeader: {
        fontSize: 18,
        fontWeight: 'bold',
        paddingTop: 5,
        paddingBottom: 5,
    },
    noDescription: {
        color: globals.colors.lightGrey,
        alignSelf: 'center',
        textAlign: 'center'
    },
    paragraph: {
        fontSize: 16,
        marginBottom: 8
    }
});

Details.propTypes = {
    actions: PropTypes.object.isRequired,
    favourite: PropTypes.bool.isRequired,
}

const mapStateToProps = (state, ownProps) => {
    let fav = state.favourites.filter((item) => {
        return item.title === ownProps.navigation.state.params.title;
    })

    console.log('fav:');
    console.log(fav);

    return {
        ...ownProps.navigation.state.params,
        favourite: fav.length > 0 ? true : false,

    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(favouritesActions, dispatch)
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Details);
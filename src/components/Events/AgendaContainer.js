import React from 'react';

import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Text, View, Button, FlatList, ListView, StyleSheet} from 'react-native';
import * as eventsActions from "../../actions/eventsAction";
import {bindActionCreators} from "redux";
import Agenda from './Agenda';

class AgendaContainer extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            showAgenda:true,
        }
        this.refreshEvents = this.refreshEvents.bind(this);
    }

    static navigationOptions = ({navigation, navigationOptions}) => {
        const {params} = navigation.state;

        return {
            title: params ? params.otherParam : 'Agenda',
            /* These values are used instead of the shared configuration! */
            headerStyle: {
                backgroundColor: navigationOptions.headerTintColor,
            },
            headerTintColor: navigationOptions.headerStyle.backgroundColor,
        };
    };

    refreshEvents() {

        this.props.actions.getEvents(this.props.config)
    }

    componentDidMount() {
        // if (this.props.events.length === 0) {
        this.props.actions.getEvents(this.props.config)

        // }
    }
    componentDidUpdate(){
    }

    shouldComponentUpdate() {
        return true;
    }

    render() {
        console.log('agenda render');
        const {config, events, actions} = this.props;

        return (

            <View style={styles.container}>

                {/*<Text>{config.PUBLIC_URL + '/api/events/project/' + config.project}</Text>*/}
                {this.state.showAgenda && events.length > 0 ? <Agenda refreshEvents={this.refreshEvents} navigation={this.props.navigation} events={events}/> : <Text />}


            </View>
        )
    }

}

AgendaContainer.propTypes = {
    events: PropTypes.array.isRequired,
    config: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,

}

const mapStateToProps = (state, ownProps) => {
    return {
        events: state.events,
        config: state.config,

    }
}

let styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(eventsActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AgendaContainer)



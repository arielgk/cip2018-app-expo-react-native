export const day1Json = [
    {
        startTime: '9:00',
        endTime: '10:00',
        title: 'Registration & Breakfast'
    },
    {
        startTime: '10:00',
        endTime: '10:30',
        title: 'Keynote',
        speaker: {
            name: 'Nick Schrock',
            company: 'Facebook',
            avatarUrl: 'http://conf.reactjs.com/img/nick-schrock.jpg'
        }
    },
    {
        startTime: '10:10',
        endTime: '11:00',
        title: 'How To Use React In a Wedding Gift Without Being A Bad Friend',
        speaker: {
            name: 'Keith Poplawski',
            company: 'Namely',
            avatarUrl: 'http://conf.reactjs.com/img/keith-poplawski.jpg'
        },
        description: [
            'As a belated gift, I’ve created a physical, standalone version of Jeopardy. Featuring React as the project’s interface, an Arduino and a node app running on a Raspberry Pi create an engaging and unique user experience. The presentation highlights React’s potential to respond to input beyond the mouse, including touch, physical buttons, and speech recognition.'
        ]
    },

];

export const day2Json = [
    {
        startTime: '9:00',
        endTime: '10:00',
        title: 'Breakfast'
    },
    {
        startTime: '10:00',
        endTime: '10:30',
        title: 'Team × Technology',
        speaker: {
            name: 'James Ide',
            company: 'Exponent',
            avatarUrl: 'http://conf.reactjs.com/img/james-ide.jpg'
        },
        description: [
            'With React Native, mobile developers are able to increase both their productivity and scope of work. The cross-platform technology is fantastic for teams building for Android and iOS, and developers can take ownership of products & features instead of single-platform implementations.',
            'At Exponent we’ve extended this idea to include both products and infrastructure. I’ll talk a bit about how we apply this to our software development and the benefits and challenges of growing full-stack developers into cross-stack mobile developers who are responsible for Android and iOS.'
        ]
    },
    {
        startTime: '10:30',
        endTime: '11:00',
        title: 'Universal GL Effects for Web and Native',
        speaker: {
            name: 'Gaetan Renaudeau',
            company: 'Project September',
            avatarUrl: 'http://conf.reactjs.com/img/gaetan-renaudeau.jpg'
        },
        description: [
            'gl-react is a universal library for implementing advanced filters and effects on top of images, videos, text or any VDOM Content (such as UI Views) - without having to deal with the imperative and low-level OpenGL and WebGL APIs. Because they are defined as simple React components, complex arrangements can be composed using the VDOM descriptive paradigm.',
            'gl-react hides OpenGL’s statefulness and exposes its greatest functional feature: GLSL (an expressive ’functional rendering’ language) and its rendering pipeline that runs on the GPU. Effects such as blur, saturation, color rotation and tinting, image composition, noise,... can all be implemented easily. But OpenGL is extremely powerful, unlocking the potential for spectacular effects and UI components: image deformation, localized blur using depth map, normal map effects, and more.'
        ]
    },
    {
        startTime: '11:00',
        endTime: '11:30',
        title: 'Break'
    },

];


import React from 'react';

import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {TextInput, Animated, Text, View, Button, TouchableOpacity, FlatList, ListView, StyleSheet} from 'react-native';
import * as eventsActions from "../../actions/eventsAction";
import {bindActionCreators} from "redux";
import {globals} from '../Globals';

class Agenda extends React.Component {

    constructor(props) {
        super(props);

        this.ds = new ListView.DataSource(
            {
                rowHasChanged: (r1, r2) => {
                    return r1 !== r2
                },
                sectionHeaderHasChanged: (s1, s2) => {
                    return s1 !== s2
                },
                getSectionHeaderData: (dataBlob, sectionID) => {
                    return sectionID
                }

            }
        );

        this.state = {
            loading: false,

            scrollDistance: 0,
            dataSource: this.ds.cloneWithRowsAndSections(this.props.events),

        }

        this.onSearchChange = this.onSearchChange.bind(this);
        this.openSearchModal = this.openSearchModal.bind(this);

    }

    onSearchChange(term) {

        if (term.length > 1) {


            console.log(term);
            let eventos;

            if (term !== '') {
                eventos = {};

                for (var key in this.state.eventos) {
                    // console.log(this.state.eventos[key]);
                    eventos[key] = this.state.eventos[key].filter(event => {
                         // console.log(event.title.toLowerCase());
                        return event.title.toLowerCase().indexOf(term.toLowerCase()) > -1 ||
                            event.authors.toLowerCase().indexOf(term.toLowerCase()) > -1  ||
                            event.simposio.toLowerCase().indexOf(term.toLowerCase()) > -1  ||
                            event.eje.toLowerCase().indexOf(term.toLowerCase()) > -1  ||
                            event.category.toLowerCase().indexOf(term.toLowerCase()) > -1  ;
                    });
                    // console.log(key);

                }

            }else{
                eventos = this.state.eventos;

            }
            // // // const eventos = this.state.eventos.map(day=>{
            // //     // return asset.name.indexOf(this.state.searchTerm) > -1;
            // //     //
            // //     return { day: day.day, events:day.events.filter(e=> {return e.title.indexOf(this.state.searchTerm) > -1;   })}
            // })
            console.log(this.state.eventos);

            console.log(eventos);
            this.setState({

                loading: false,
                scrollDistance: 0,
                dataSource: this.ds.cloneWithRowsAndSections(eventos)

            })
        }

    }

    openSearchModal() {

    }

    shouldComponentUpdate() {
        return true;
    }

    componentDidMount() {

        let eventos = {};

        const evts = this.props.events.map((day, index) => {
                eventos[day.day] = day.events;
                return day.events
            }
        );

        this.setState({

            loading: false,
            scrollDistance: 0,
            eventos: eventos,
            dataSource: this.ds.cloneWithRowsAndSections(eventos)

        })

    }

    render() {

        return (
            <View>

                {/*<Button title="Buscar"*/}
                {/*onPress={() => this.openSearchModal}/>*/}
                {/*<Button onPress={()=>this.props.refreshEvents()} title="Recargar" />*/}
                {/**/}
                <ListView
                    // style={styles.list}
                    dataSource={this.state.dataSource}
                    renderRow={this.renderRow.bind(this)}
                    renderSectionHeader={this.renderSectionHeader.bind(this)}
                    automaticallyAdjustContentInsets={false}/>

                <View style={styles.searchModal}>
                    <TextInput
                        style={styles.searchTextBox}
                        placeholder="Search"
                        editable={true}
                        onChangeText={this.onSearchChange}
                    />
                </View>

            </View>
        );
    }

    renderRow(rowData, sectionID, rowID) {

        let title = <Text style={[styles.title, styles.titleSingle]}>{rowData.title}</Text>;
        let circleStyle;
        let content;

        content = (
            <TouchableOpacity onPress={() => this._onRowPressed(rowData)} key={rowID}>

                <View style={styles.row}>
                    <View style={styles.timeContainer}>
                        <Text style={styles.timeText}>{rowData.startTime}</Text>
                    </View>
                    <View style={styles.details}>
                        <View style={[styles.circle, circleStyle]}></View>
                        {title}
                        <View style={styles.category}>
                            <Text style={styles.categoryText}>{rowData.category}</Text>
                        </View>
                        <View style={styles.category}>
                            <Text style={styles.categoryText}>{rowData.venue_name}</Text>
                        </View>

                        <View style={styles.separator}></View>


                    </View>
                </View>
            </TouchableOpacity>
        );

        return (
            <View key={rowID}>
                {content}
            </View>
        );
    }

    renderSectionHeader(sectionData, sectionID) {
        let scrollDistance = this.state.scrollDistance;

        return (

            <Animated.View style={styles.sectionHeader}>
                <Text style={styles.sectionHeaderText}>
                    {sectionData}
                </Text>
            </Animated.View>
        );
    }

    _onRowPressed(rowData) {
        // let route = {
        //     name: 'details',
        //     title: 'Talk Details',
        //     talkInfo: rowData
        // };
        //
        // this.props.navigator.push(route);

        this.props.navigation.push('Details', rowData);
    }

}

Agenda.propTypes = {
    events: PropTypes.array.isRequired,
    config: PropTypes.object.isRequired,

}

const mapStateToProps = (state, ownProps) => {
    return {
        events: state.events,
        config: state.config,

    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(eventsActions, dispatch)
    }
}

let styles = StyleSheet.create({
    container: {
        flex: 1
    },
    // ListView
    list: {
        flex: 1
    },

    searchModal: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        backgroundColor: "#fff",
        width: '100%',
        padding: 10,
    },
    searchTextBox: {
        fontSize: 16,
    },
    // SectionHeader
    category: {

        paddingTop: 5,

    },
    categoryText: {
        fontSize: 12,
        color: globals.colors.primary,
    },
    sectionHeader: {

        marginBottom: 15,
        backgroundColor: globals.colors.secondary,
        height: 35,
        justifyContent: 'center'
    },
    sectionHeaderText: {
        color: '#FFF',
        fontSize: 18,
        alignSelf: 'center',
        marginTop: 5,
    },
    // Row
    row: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'center',
        paddingLeft: 15,
        paddingRight: 15
    },
    timeContainer: {
        width: 40
    },
    timeText: {
        color: globals.colors.lightGrey,
        textAlign: 'right'
    },
    circle: {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: '#88C057',
        position: 'absolute',
        left: -5,
        top: 0
    },
    details: {
        borderColor: globals.colors.lightGrey,
        borderLeftWidth: 1,
        flexDirection: 'column',
        flex: 1,
        marginLeft: 20,
        paddingLeft: 20
    },
    title: {
        fontSize: 16,
        fontWeight: '600',
        color: globals.colors.primary,
        marginBottom: 6
    },
    titleSingle: {
        marginBottom: 0
    },
    speakerInfo: {
        flexDirection: 'row'
    },
    speakerAvatar: {
        width: 40,
        height: 40,
        borderRadius: 20,
        marginRight: 15,
    },
    speakerDescription: {
        flex: 1
    },
    separator: {
        height: .5,
        backgroundColor: globals.colors.lightGrey,
        marginTop: 15,
        marginBottom: 15
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Agenda)



import React from 'react';
import PropTypes from 'prop-types';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as articleActions from "../../actions/articlesActions";
import Articles from '../Articles';


const styles=  {

}
class News extends React.Component {
    static navigationOptions = ({navigation, navigationOptions}) => {
        const {params} = navigation.state;

        return {
            title: params ? params.otherParam : 'Noticias',
            /* These values are used instead of the shared configuration! */
            headerStyle: {
                backgroundColor: navigationOptions.headerTintColor,
            },
            headerTintColor: navigationOptions.headerStyle.backgroundColor,
        };
    };


    componentDidMount(){
        if(this.props.articles.length === 0){
            this.props.actions.getArticles(this.props.config)

        }
    }
    render() {


        const {config, articles, actions} = this.props;
        return (


                <Articles articles={articles}/>



        );
    }
}

News.propTypes = {
    config: PropTypes.object.isRequired,
    articles: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired,
}

const mapStateToProps = (state, ownProps) => {
    return {
        config: state.config,
        articles: state.articles,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(articleActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(News)

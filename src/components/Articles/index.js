import React from 'react';
import Article from "./Article";
import {connect} from 'react-redux';


import {Text, View, Button, FlatList} from 'react-native';

class Articles extends React.Component {
    render() {
        const {articles} = this.props
        return (
            <FlatList
                data={articles}
                renderItem={item =>  <Article key={item.id}    article={item}/>}

                keyExtractor={(item, index) => item.id.toString()}
            >
            </FlatList>
        )


    }
}

export default Articles
import React from 'react';
import PropTypes from 'prop-types';
import {ActivityIndicator,StyleSheet, Text, View, Image, Dimensions} from 'react-native';
import {connect} from 'react-redux';
import ResponsiveImageView from 'react-native-responsive-image-view';

class Article extends React.Component {
    render() {

        const {article, config} = this.props;
        console.log(config.BASE_URL + '/img/' + article.item.featured_image);
        return (
            <View style={styles.content}>

                <View style={styles.constrainingContainer}>

                <ResponsiveImageView source={{uri: config.PUBLIC_URL + '/img/' + article.item.featured_image}}>
                    {({loading,getViewProps, getImageProps}) => {
                        if (loading) {
                            return <ActivityIndicator style={styles.activityIndicatorImage}animating={true} size="large" />;
                        }
                     return(
                         <View {...getViewProps()}>
                            <Image {...getImageProps()} />
                        </View>
                     )
                    }}

                </ResponsiveImageView>

                {/*<Image*/}


                {/*source={{uri: config.PUBLIC_URL + '/img/' + article.item.featured_image}}*/}
                {/*/>*/}
                <Text style={styles.articleHeader}>
                    {article.item.title}
                </Text>
            </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    constrainingContainer: {

        // height: '66%', // play with this value
        width: '100%', // play with this value
    },
    articleHeader:{
        fontSize:25,
        paddingTop:5,
        paddingLeft:5,
        paddingRight:5,
        paddingBottom:5,
    },
    imageContainer: {
        backgroundColor: '#fff',
    },
    activityIndicatorImage:{
        paddingTop:25,
        paddingBottom:25,
    }

});

Article.propTypes = {
    article: PropTypes.object.isRequired,
    config: PropTypes.object.isRequired,

}

const mapStateToProps = (state, ownProps) => {
    return {
        article: ownProps.article,
        config: state.config,

    }
}

export default connect(mapStateToProps)(Article);

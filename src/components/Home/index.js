import React from 'react';
import PropTypes from 'prop-types';
import {ScrollView, TouchableOpacity, StyleSheet, Text, View, Button, Image} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as postsActions from "../../actions/postsActions";
import Favourite from './Favourite';
import CipHero from '../../../assets/CIP2018-logo.jpg';
import untref from '../../../assets/untref.png';
import sep from '../../../assets/sep_logo.png';
import calendarIcon from '../../../assets/calendar-alt-regular.png';
import mapIcon from '../../../assets/map-solid.png';
import Map from '../Maps';
import { AppLoading, Asset, Font, Icon } from 'expo';


class Home extends React.Component {

    static navigationOptions = ({navigation, navigationOptions}) => {
        const {params} = navigation.state;

        return {
            title: params ? params.otherParam : 'CIP2018',
            /* These values are used instead of the shared configuration! */
            headerStyle: {
                backgroundColor: navigationOptions.headerTintColor,
            },
            headerTintColor: navigationOptions.headerStyle.backgroundColor,
        };
    };

    constructor(props) {
        super(props);

        this.state = {
            showMaps: false,
        }

        this.toggleMap = this.toggleMap.bind(this);
    }

    willFocus = this.props.navigation.addListener(
        'willFocus',
        payload => {
            this.forceUpdate();
        }
    );

    toggleMap() {
        this.setState({
            showMaps: !this.state.showMaps,
        })

        console.log(this.state.showMaps);
    }

    componentDidMount() {
        this.props.actions.getPosts(this.props.config);

    }

    render() {


        if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
            return (
                <AppLoading
                    startAsync={this._loadResourcesAsync}
                    onError={this._handleLoadingError}
                    onFinish={this._handleFinishLoading}
                />
            );
        } else {
            const {config, sections} = this.props;
            return (

                <ScrollView style={styles.container}>
                    <View style={styles.headerBar}>
                        <View style={styles.logosContainer}>
                            <View style={styles.logoContainer}>
                                <Image style={styles.untrefImage} source={untref} resizeMode="contain"
                                />
                            </View>
                            <View style={styles.logoContainer}>
                                <Image style={styles.sepImage} source={require('../../../assets/sep_logo.png')}
                                       resizeMode="contain"
                                />
                            </View>
                        </View>

                    </View>


                    <View style={styles.scrollView}>


                        <View style={styles.imageHeroContainer}>
                            <Image style={styles.imageHeroImage} source={CipHero} resizeMode="contain"
                            />
                        </View>


                        <View>
                            <Text style={styles.bajada}>“La innovación y el futuro de la educación para un mundo
                                plural”</Text>
                        </View>
                        <View style={styles.menuHome}>
                            <TouchableOpacity onPress={() => this.props.navigation.push('Events')}
                                              style={styles.menuButton}>
                                <View style={styles.menuIconContainer}>
                                    <Image style={styles.iconButton} source={calendarIcon} resizeMode="contain"/>
                                </View>

                                <Text style={styles.menuButtonText}>
                                    Agenda
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.toggleMap()} style={styles.menuButton}>
                                <View style={styles.menuIconContainer}>
                                    <Image style={styles.iconButton} source={mapIcon} resizeMode="contain"/>
                                </View>
                                <Text style={styles.menuButtonText}>
                                    Mapa
                                </Text>
                            </TouchableOpacity>


                        </View>


                        <View style={styles.favouriteBar}>
                            <View style={styles.favouriteLabel}>
                                <Text style={styles.favouriteLabelText}>Anuncios </Text>

                            </View>
                            <View stinyle={styles.favouriteDots}>

                            </View>
                        </View>
                        <View style={styles.postResultContainer}>
                            {this.props.posts.length == 0 ? <Text>No hay anuncios recientes</Text> : <Text/>}
                            {this.props.posts.length > 0 ?
                                <View>
                                    {this.props.posts.map((post, index) => {
                                        console.log(post);
                                        return (
                                            <View style={styles.postRow} key={'post' + index}>
                                                <Text style={styles.postTitle}>{post.title}</Text>
                                                <Text style={styles.postContent}>{post.content}</Text>
                                            </View>)

                                    })
                                    }
                                </View> : <Text/>}
                        </View>

                        <View style={styles.favouriteBar}>
                            <View style={styles.favouriteLabel}>
                                <Text style={styles.favouriteLabelText}>Eventos </Text>
                                <Text style={styles.favouriteLabelTextLight}>Favoritos</Text>
                            </View>
                            <View stinyle={styles.favouriteDots}>

                            </View>
                        </View>
                        {this.props.favourites.length == 0 ?
                            <Text>No tienes eventos favoritos todavia</Text> : <Text/>}
                        {this.props.favourites.map((fav, index) => <Favourite favourite={fav} index={index}
                                                                              key={'fav' + index}/>)}
                    </View>
                    <View>


                        {
                            this.state.showMaps ? <Map toggleMap={this.toggleMap}/> : <Text/>
                        }
                    </View>

                </ScrollView>
            );

        }

    }

    _loadResourcesAsync = async () => {
        return Promise.all([
            Asset.loadAsync([
                require('../../../assets/sep_logo.png'),
                require('../../../assets/untref.png'),
            ]),
            // Font.loadAsync({
                // This is the font that we are using for our tab bar
                // ...Icon.Ionicons.font,
                // We include SpaceMono because we use it in HomeScreen.js. Feel free
                // to remove this if you are not using it in your app
                // 'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
            // }),
        ]);
    };

    _handleLoadingError = error => {
        // In this case, you might want to report the error to your error
        // reporting service, for example Sentry
        console.warn(error);
    };

    _handleFinishLoading = () => {
        this.setState({isLoadingComplete: true});
    };

}

Home.propTypes = {
    config: PropTypes.object.isRequired,
    sections: PropTypes.array.isRequired,
    favourites: PropTypes.array.isRequired,
    posts: PropTypes.array.isRequired,
}

const mapStateToProps = (state, ownProps) => {
    return {

        config: state.config,
        sections: state.sections,
        favourites: state.favourites,
        posts: state.posts,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(postsActions, dispatch)
    }
}

let styles = StyleSheet.create({
        container: {
            flex: 1,

            backgroundColor: '#fff',
            height: '100%',
        },
        scrollView: {
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            padding: 20,
        },
        headerBar: {
            backgroundColor: '#6E8AC2',
            height: 100,
        },
        logosContainer: {
            flex: 1,
            flexDirection: 'row',

        },
        logoContainer: {
            paddingLeft: 20,

        },
        untrefImage: {
            width: 100,
            height: 100,

        },
        sepImage: {
            width: 80,
            height: 100,
        },
        imageHeroContainer: {
            // padding:20,
            flex: 1,
            flexDirection: 'row',

        },
        imageHeroImage: {
            flex: 1,
            alignSelf: 'stretch',

        },
        bajada: {
            fontSize: 15,
            fontStyle: 'italic',
        },
        menuHome: {
            flex: 1,
            flexDirection: 'row',
            padding: 10,
            marginTop: 30,

        },
        menuButton: {
            width: '50%',
            backgroundColor: '#6E8AC2',
            margin: 10,
            height: 110,

        },
        menuIconContainer: {
            flex: 1,
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center',
            maxHeight: 70,
        },
        menuButtonText: {
            textAlign: 'center',
            fontSize: 20,
            color: '#fff',
            alignSelf: 'center',

        },
        iconButton: {
            flex: 1,
            // alignSelf: 'stretch',
            width: 30,

        },
        circle: {
            width: 10,
            height: 10,
            borderRadius: 5,
            backgroundColor: '#6E8AC2',
            left: 0,
            top: 0

        },
        postResultContainer: {
            flex: 1,
        },
        postRow: {
            flex: 1,
            flexDirection: "row",
            borderBottomColor: '#ccc',
            borderBottomWidth: 1,
            paddingBottom: 10,
            paddingTop: 5,
            flexWrap: 'wrap'
        },
        postTitle: {
            width: '100%',
            fontWeight: 'bold',
        },
        postContent: {
            width: '100%',
        },
        favouriteBar: {
            marginBottom: 10,
            flex: 1,
            flexDirection: 'row',
            maxHeight: 25,
            borderBottomColor: '#6E8AC2',
            borderBottomWidth: 2,
            marginTop: 30,
        },
        favouriteDots: {
            backgroundColor: '#fff',

            fontSize: 14,
            padding: 4,
            paddingBottom: 5,
            textAlign: 'right',
            width: '50%',

        },
        favouriteLabel: {
            backgroundColor: '#6E8AC2',

            padding: 4,
            paddingBottom: 5,
            width: '50%',
            flex: 1,
            flexDirection: 'row',

        },
        favouriteLabelTextLight: {
            fontSize: 14,
            textAlign: 'center',
            color: "#fff",

        },
        favouriteLabelText: {
            fontSize: 14,
            textAlign: 'center',
            color: "#fff",
            fontWeight: 'bold',

        },
    }
)
export default connect(mapStateToProps, mapDispatchToProps)(Home)

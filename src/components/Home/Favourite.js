import React from 'react';
import {ScrollView, Alert, TouchableOpacity, StyleSheet, Text, View, Button} from 'react-native';
import * as favouritesActions from "../../actions/favouritesActions";
import {bindActionCreators} from "redux";
import {connect} from 'react-redux';

class Favourite extends React.Component {
    constructor(props) {
        super(props)
        this.onDeleteFavourite = this.onDeleteFavourite.bind(this);
    }

    onDeleteFavourite() {
        console.log('asd');
        Alert.alert(
            'Borrar Favorito',
            'Esta Seguro?',
            [
                {text: 'No', onPress: () => console.log('Cancel Pressed')},
                {
                    text: 'Si', onPress: () => this.props.actions.deleteFavourite({
                        index: this.props.index
                    })
                },

            ],
            {cancelable: true}
        )
    }

    render() {
        const {favourite, actions} = this.props;
        return (
            <TouchableOpacity style={styles.favouritesRow} onPress={() => {
                this.onDeleteFavourite()
            }}>
                <Text style={styles.favouritesTitle}>{favourite.title}</Text>
                <View style={styles.favouritesTime}>
                    <Text style={styles.favouritesStartTime}>{favourite.startTime}</Text>
                    <Text style={styles.favouritesEndTime}>{favourite.endTime}</Text>
                    <Text style={styles.favouriteVenue}>{favourite.venue_name}</Text>
                </View>


            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    favouritesRow: {
        flex: 1,
        flexDirection: "row",
        borderBottomColor: '#ccc',
        borderBottomWidth: 1,
        paddingBottom: 10,
        paddingTop: 5,
    },
    favouritesTitle: {
        width: '70%',
    },
    favouritesTime: {
        width: '30%',
        flex: 1,
        flexDirection: 'row',
        flexWrap:'wrap',

    },
    favouritesStartTime: {
        width: '50%',
        fontSize: 13,

    },
    favouritesEndTime: {
        width: '50%',
        fontSize: 13,

    },
    favouriteVenue:{
        textAlign:'left',
        marginTop:5,
        width:'100%',
    }

})

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(favouritesActions, dispatch)
    }
}
export default connect(null, mapDispatchToProps)(Favourite);

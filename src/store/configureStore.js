import {createStore, applyMiddleware, compose} from "redux";
import rootReducer from "../reducers/index";
import thunk from "redux-thunk";
import { persistStore, persistReducer } from 'redux-persist'
import autoMergeLevel1 from 'redux-persist/lib/stateReconciler/autoMergeLevel1'


import storage from 'redux-persist/lib/storage' // defaults to localStorage for web and AsyncStorage for react-native

import devTools from 'remote-redux-devtools';

// or const store = createStore(reducer, preloadedState, devToolsEnhancer());

// import initialState from './initialState';
// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


// In case you don't set NODE_ENV, you can set realtime
// parameter to true or to other global variable to turn it off in production:


// const composeEnhancers = composeWithDevTools({realtime: true, port: 8000});

// export default function configureStore(initialState) {
//     return createStore(
//         rootReducer,
//         initialState,
//         applyMiddleware(thunk),
//         devTools({
//             name: 'ios',
//             hostname: 'localhost',
//             port: 5678
//         })
//
//
//     );
// }

const persistConfig = {
    key: 'root',
    storage,
    blacklist: ['navigation','config'], // navigation will not be persisted
    stateReconciler: autoMergeLevel1,

}


const persistedReducer = persistReducer(persistConfig, rootReducer)

export default function configureStore(initialState) {
    // const enhancer = compose(
    //     applyMiddleware(thunk),
    //     devTools({
    //         name: 'IOS',
    //         hostname: 'localhost',
    //         port: 5678
    //     }),
    //
    // );


    const store = createStore(
        persistedReducer,
        {},
        compose(
            applyMiddleware(thunk),
            // devTools({
            //     name: 'IOS',
            //     hostname: 'localhost',
            //     port: 5678
            // }),
        )
    )

    let persistor = persistStore(store);


    // console.log(persistor);
    return {store, persistor}
}


// const store = createStore(
//     combineReducers(reducers),
//     {},
//     compose(
//         autoRehydrate(),
//         applyMiddleware(thunk)
//     )
// )

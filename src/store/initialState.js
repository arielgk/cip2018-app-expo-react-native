export default {
    ajaxCallInProgress: 0,
    config: {
        PUBLIC_URL: "https://eventos.untref.edu.ar",
        BASE_URL: "https://eventos.untref.edu.ar/",
        project: 1,

        initialRouteName: 'Home',
        navigationOptions: {
            headerStyle: {
                backgroundColor: '#505050',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {

                fontWeight: 'bold',
            },
        },
        colors: {
            primary: '#3B3738',
            secondary: '#05A9D6',
            lightGrey: '#AAA'
        }
    },
    articles: [],
    sections: [
        {
            name: "news",
            label: "Noticias",
            order: 0,
        },
        {
            name: "events",
            label: "Eventos",
            order: 0,
        },

    ],
    events: [],
    favourites: [
        // {
        //     title: "PROCESO DE INTERNACIONALIZACIÓN DE LA UNIVERSIDAD NACIONAL DE ITAPÚA.",
        //     startTime: "09:00",
        //     endTime: "10:30",
        //     start: "2018-08-14 09:00:00",
        //     venue_name:"Salon de los caidos",
        // }
        // ,
        // {
        //     title: "PROCESO DE INTERNACIONALIZACIÓN DE LA UNIVERSIDAD NACIONAL DE ITAPÚA.",
        //     startTime: "09:00",
        //     endTime: "10:30",
        //     start: "2018-08-14 09:00:00",
        //     venue_name:"Aula 323",
        // }
    ],
    posts: [
        {
            id: 1,
            content: "contenido",
            title: "titulo",
        }
    ],

};
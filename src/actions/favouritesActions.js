import * as types from "./actionTypes";

export const setFavourite = (event) => {

    return dispatch => {
        dispatch({type: types.SET_FAVORITE, favourite: event});
    };

}

export const deleteFavourite = (favouriteIndex)=>{
    return dispatch =>{
        dispatch({type:types.DELETE_FAVORITE, favouriteIndex:favouriteIndex});
    }
}

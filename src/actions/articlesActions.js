import * as types from "./actionTypes";
import axios from 'axios';
import {beginAjaxCall} from "./ajaxStatusActions";

export const getArticles = (config) => {
    console.log('get')
    return dispatch => {
        dispatch(beginAjaxCall());

        axios({
            method: 'get',
            url: config.PUBLIC_URL + '/api/articles/project/' + config.project,
            responseType: 'json'
        })
            .then(function (response) {
                console.log(response);

                dispatch({type: types.GET_ARTICLES_SUCCESS, articles: response.data});
            });

        //add error

    }
}
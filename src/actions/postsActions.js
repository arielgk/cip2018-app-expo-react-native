import * as types from "./actionTypes";
import axios from 'axios';
import {beginAjaxCall} from "./ajaxStatusActions";

export const getPosts= (config) => {

    return dispatch => {
        dispatch(beginAjaxCall());

        axios({
            method: 'get',
            url: config.PUBLIC_URL + '/api/posts/project/' + config.project,
            responseType: 'json'
        })
            .then(function (response) {

                console.log('done');
                dispatch({type: types.GET_POSTS_SUCCESS, posts: response.data});
            });



    }
}
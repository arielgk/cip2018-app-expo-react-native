import * as types from "./actionTypes";
import axios from 'axios';
import {beginAjaxCall} from "./ajaxStatusActions";

export const getEvents= (config) => {

    return dispatch => {
        dispatch(beginAjaxCall());

        axios({
            method: 'get',
            url: config.PUBLIC_URL + '/api/events/project/' + config.project,
            responseType: 'json'
        })
            .then(function (response) {

                console.log('done');
                dispatch({type: types.GET_EVENTS_SUCCESS, events: response.data});
            });



    }
}
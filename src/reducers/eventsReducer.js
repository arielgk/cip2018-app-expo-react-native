import * as types from "../actions/actionTypes";
import initialState from "../store/initialState";

const events = (state = initialState.events, action) => {
    switch (action.type) {
        case types.GET_EVENTS_SUCCESS:
            return action.events;
        default:
            return state;
    }
};

export default events;

import * as types from "../actions/actionTypes";
import initialState from "../store/initialState";

const favourites = (state = initialState.favourites, action) => {
    switch (action.type) {
        case types.SET_FAVORITE:
            console.log(state);
            return [
                ...state.slice(0,1),
                action.favourite,
                ...state.slice(1)
            ]
        case types.DELETE_FAVORITE:
            console.log(action.favouriteIndex);
            return [
                ...state.slice(0, action.favouriteIndex.index),
                ...state.slice(action.favouriteIndex.index + 1)
            ];
        default:
            return state;
    }
};

export default favourites;

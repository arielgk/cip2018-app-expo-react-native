import * as types from "../actions/actionTypes";
import initialState from "../store/initialState";

const sections = (state = initialState.sections, action) => {
    switch (action.type) {
        default:
            return state;
    }
};

export default sections;

import * as types from "../actions/actionTypes";
import initialState from "../store/initialState";

const Config = (state = initialState.config, action) => {
    switch (action.type) {
        default:
            return state;
    }
};

export default Config;

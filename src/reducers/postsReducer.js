import * as types from "../actions/actionTypes";
import initialState from "../store/initialState";

const posts = (state = initialState.posts, action) => {
    switch (action.type) {
        case types.GET_POSTS_SUCCESS:
            return action.posts;
        default:
            return state;
    }
};

export default posts;

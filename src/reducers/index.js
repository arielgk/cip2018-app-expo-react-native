import {combineReducers} from "redux";
import ajaxCallInProgress from "./ajaxStatusReducer";
import config from "./configReducer";
import articles from "./articlesReducer";
import events from "./eventsReducer";
import sections from "./sectionsReducer";
import favourites from "./favouritesReducer";
import posts from "./postsReducer";


const rootReducers = combineReducers({
    ajaxCallInProgress,
    config,
    articles,
    sections,
    events,
    favourites,
    posts,
});

export default rootReducers;
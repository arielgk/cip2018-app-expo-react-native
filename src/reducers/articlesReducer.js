import * as types from "../actions/actionTypes";
import initialState from "../store/initialState";

const articles = (state = initialState.articles, action) => {
    switch (action.type) {
        case types.GET_ARTICLES_SUCCESS:
            console.log(action.articles);
            return action.articles;
        default:
            return state;
    }
};

export default articles;
